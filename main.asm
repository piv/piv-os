%include "vars.asm"

; 230702 f: there is a strange bug where, if you type
;           enough prompts for it to start scrolling
;           the screen down, the first character will
;           get cut off from the echoed prompt. i am
;           Inexperienced:tm: so have no clue why this
;           may be
;           maybe dl is overflowing into dh? or not. i
;           may be entirely incorrect. i literally dont
;           dont know.
; 230703 o: apparently my hacky `newline` routine was in
;           fact wrong somehow. outputting \r\n instead
;           through int 10h al = 0Eh fixed the bug.
; 230703 f: i made some progress on the lexer; like, it
;           doesn't... *work* yet, but we're getting
;           there. location 230703.1 needs to be changed
;           because presently it adds a separator between
;           every *char*, not token, and location 230703.2
;           needs to be... written.
; 230709 o: i have relocated everything.

org KERNEL_LOCATION
bits 16

mov ax, INPUT_BUFFER ; input buffer
mov es, ax
mov ds, ax

mov ax, 0x0200 ; move cursor
mov dx, 0x0000 ; row 0 column 0
int VIDEO_INTERRUPT

read:
  mov di, 0x0001
  mov bh, 0x00 ; page 0

  mov ah, 0x0E ; write character
  mov al, ' '
  int VIDEO_INTERRUPT
  mov al, '$'
  int VIDEO_INTERRUPT
  mov al, ' '
  int VIDEO_INTERRUPT

.tight_loop:
  mov ah, 0x00 ; ah = 0x00, read keypress, blocking
  int KEYBOARD_INTERRUPT
  mov ah, 0x0E ; al = ASCII, ah = scancode.
               ; override ah = 0x0E to write a char
  int VIDEO_INTERRUPT ; note: when al is CR, this moves
  stosb               ; the cursor to the left side of the screen
  cmp al, 13   ; check if char is RETURN (enter); if yes,
               ; continue to eval stage; regardless, echo
               ; character to the screen
  jne .tight_loop
  mov al, 10 ; output LF to complete the CRLF output.
             ; this will move the cursor down one line.
  int VIDEO_INTERRUPT

eval:
  mov [TOKENS], di ; save location of token string
  mov si, 0x0001 ; set ds:si to the start of the input buffer;
                 ; keep es:di where it is

%include "iv/parsing.asm"

;  jmp read

; ote the sunki your routine is about to be modified watch out
print:
  mov si, [TOKENS] ; move ds:si to start of token string
  mov ah, 0x0E ; write character
  mov bh, 0x00 ; page 0
  .tight_loop:
    lodsb
    cmp al, 0
    je .out
    int VIDEO_INTERRUPT
    cmp al, 10
    jne .tight_loop

    mov al, 13 ; output CR to complete LFCR. the ole switcheroo
    int VIDEO_INTERRUPT
    jmp .tight_loop

  .out:
  mov al, 13
  int VIDEO_INTERRUPT
  mov al, 10
  int VIDEO_INTERRUPT
  jmp read

jmp $ ; stall

; data
TOKENS: dw 0

times 2560-($-$$) db 0
