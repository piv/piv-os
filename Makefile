all: os.iso

%.bin : %.asm
	nasm -f bin -o $@ $^

os.iso : boot.bin main.bin
	cat boot.bin main.bin > os.iso

qemu : os.iso
	qemu-system-x86_64 -drive format=raw,file=os.iso -full-screen -no-reboot

clean:
	rm boot.bin
	rm main.bin
