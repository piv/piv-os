%include "vars.asm"

org BOOTLOADER_LOCATION
bits 16

mov ax, 0x0002      ; enable video mode 0x02
int VIDEO_INTERRUPT ; 16 colors, 80x25 text mode

mov ax, STACK_SEGMENT  ; put the stack in a
mov ss, ax             ; not batshit location
mov sp, STACK_POINTER  ; (free memory just before bootloader)

mov ax, KERNEL_SEGMENT  ; load next part of
mov es, ax              ; bootloader at es:bx
mov bx, KERNEL_POINTER  ; bx + KERNEL_SIZE * 0x0200 must < 0x10000.

mov ax, 0x0204  ; ah = 0x02 read sectors
                ; al = 0x04 sectors read
mov cx, 0x0002  ; cx = cylinder 0, sector 2
mov dh, 0x00    ; dh = head number 0
                ; dl = set by BIOS, current disk drive
int IO_INTERRUPT

jmp KERNEL_SEGMENT:KERNEL_POINTER ; jump to main.asm

times 510-($-$$) db 0
dw 0xaa55         ; boot signature
