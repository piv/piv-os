; 230704 f: initial whatever;
;           generates a null terminated string of
;           tokens separated by newlines
;           presently does not distinguish between words
;           and numbers
;           planning to probably represent that with a
;           leading byte before every token describing
;           what type of token it is

  mov ax, 0x0A00 ; \n (0Ah) is used to delimit tokens;
                 ; zeroth char should be anything but 20h
  mov ch, '#' ; literal type byte
  mov bx, di ; bx is used to represent location of type byte
             ; '#' = literal
             ; '$' = word

  mov al, '#'
  stosb ; store first type byte
.parse:
  mov cl, al ; last processed character
  lodsb

  cmp al, 32 ; check for whitespace; if found, skip char
  je .parse

  cmp al, 13 ; if char is \r, we've reached the end of
  je .fin    ; the prompt; break loop

  cmp cl, 32  ; no separator if either a) first token or
  jne .parse1 ; b) last character was non-whitespace

  xchg al, ah
  stosb ; write newline
  xchg al, ah

  mov bx, di
  xchg ch, al
  stosb
  xchg ch, al

.parse1:
  stosb ; store char
  cmp al, '0'
  jb .isword
  cmp al, '9'
  ja .isword

  jmp .parse

.isword:
  mov byte [es:bx], '$'

  jmp .parse

.fin:
  xor al, al
  stosb
