# ![PIV](piv.svg)

itsy bitsy 16-bit forth os inspired by jonesforth

developed by a [Hexagon](https://codeberg.org/ufrag/) and a [Tomato](https://codeberg.org/otesunki/)

present specifications:
- interaction via forth prompt
- uses forth variant named `iv`
- 16-bit real mode
- text mode, 0x02
    * 16 color
    * 80x25 characters
- no... actual filesystem, flat array of Thing
- no kernel. screw kernels! who needs em?
- written in RAW X86 ASSEMBLY!! OOOOO!!! SCARY!!!!
